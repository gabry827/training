package training.test;

import org.junit.Test;
import training.HourGlass;
import training.HourGlassesUtils;

import static org.junit.Assert.assertEquals;

public class HourGlassesUtilsTest {

    private HourGlassesUtils sut = new HourGlassesUtils();

    private static final int[][] testMatrix1 = {
            {-9, -9, -9, 1, 1, 1},
            {0, -9, 0, 4, 3, 2},
            {-9, -9, -9, 1, 2, 3},
            {0, 0, 8, 6, 6, 0},
            {0, 0, 0, -2, 0, 0},
            {0, 0, 1, 2, 4, 0}
        };

    private static final int[][] testMatrix2 = {
            {1, 1, 1, 0, 0, 0},
            {0, 1, 0, 0, 0, 0},
            {1, 1, 1, 0, 0, 0},
            {0, 0, 2, 4, 4, 0},
            {0, 0, 0, 2, 0, 0},
            {0, 0, 1, 2, 4, 0}
    };

    @Test
    public void getMaxHourGlass1() {
        HourGlass result = sut.getMaxHourGlass(testMatrix1);
        assertEquals(28, result.getValue());
        assertEquals(1, result.getRowIndex());
        assertEquals(2, result.getColIndex());
    }

    @Test
    public void getMaxHourGlass2() {
        HourGlass result = sut.getMaxHourGlass(testMatrix2);
        assertEquals(19, result.getValue());
    }
}