package training;

public class HourGlassesUtils {

    public HourGlass getMaxHourGlass(int[][] table){
        HourGlass maxHourGlass = null;

        for (int row = 0; row < table[0].length; row++){
            for (int col = 0; col < table.length; col++){
                maxHourGlass = getHourGlass(table, row, col, maxHourGlass);
            }
        }

        return maxHourGlass;
    }

    private HourGlass getHourGlass(int[][] table, int row, int col, HourGlass maxHourGlass) {
        int currentValue;

        try {
            currentValue = getValueSafe(table, row, col) + getValueSafe(table, row, col + 1) + getValueSafe(table, row, col + 2) + getValueSafe(table, row + 1, col + 1) + getValueSafe(table, row + 2, col) + getValueSafe(table, row + 2, col + 1) + getValueSafe(table, row + 2, col + 2);
        } catch (OutOfBoundHourGlassException e) {
            return maxHourGlass;
        }

        if (maxHourGlass == null || currentValue >= maxHourGlass.getValue()){
            return new HourGlass(row, col, currentValue);
        }

        return maxHourGlass;
    }

    private int getValueSafe(int[][] table, int row, int col) throws OutOfBoundHourGlassException {
        try {
            return table[row][col];
        } catch (IndexOutOfBoundsException e) {
            throw new OutOfBoundHourGlassException();
        }
    }
}
